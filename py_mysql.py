# sudo pip3 install sqlalchemy pymysql
from sqlalchemy import create_engine
import pymysql

con = create_engine('mysql+pymysql://guest:relational@relational.fit.cvut.cz:3306/imdb_small')

print(con.table_names())

r = con.execute("select * from movies limit 5;").fetchall()
print(r)