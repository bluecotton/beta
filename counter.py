from tkinter import *

male_cnt = 0
female_cnt = 0
total_cnt = 0


def male_click():
    global male_cnt
    global total_cnt
    male_cnt += 1
    total_cnt += 1
    tv_male.set(male_cnt)
    tv_total.set('total = {}'.format(total_cnt))


def female_click(e):
    global female_cnt
    global total_cnt
    female_cnt += 1
    total_cnt += 1
    tv_female.set(female_cnt)
    tv_total.set('total = {}'.format(total_cnt))



root = Tk()
root.option_add("*Font", "consolas 30")

tv_male = IntVar()
tv_female = IntVar()
tv_total = StringVar()

# color name: https://wiki.tcl.tk/37701
Button(root, text="male", bg="deep sky blue", width=7,
       command=male_click).grid(row=0,
                                column=0,
                                padx=10,
                                ipady=25)
# for macOS use 'highlightbackground' to change button backgound color
# Button(root, text="male", highlightbackground="deep sky blue", width=7, command=male_click).grid(row=0, column=0, padx=10, ipady=25)

btn_female = Button(root, text="female", bg="hot pink", width=7)
# for macOS use 'highlightbackground' to change button backgound color
# btn_female = Button(root, text="female", highlightbackground="hot pink", width=7)
btn_female.grid(row=0, column=1, padx=10, ipady=25)
btn_female.bind('<Button-1>', female_click)

Label(root, textvariable=tv_male, bg="deep sky blue").grid(row=1, column=0, padx=10,
                                                           sticky="news")
Label(root, textvariable=tv_female, bg="hot pink").grid(row=1, column=1, padx=10,
                                                        sticky="news")
Label(root, textvariable=tv_total, bg="gold").grid(row=2, columnspan=2, padx=10,
                                                   sticky="news", pady=20)

root.mainloop()
